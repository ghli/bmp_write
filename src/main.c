#include <argp.h>
#include <stdlib.h>
#include <string.h>
#include "bmpwriter.h"

struct argp_option opts[] = {
    {"output",  'o',    "FILE",     0,  "Output filename"},
    {"depth",   'c',    "DEPTH",    0,  "Depth of the bitmap"},
    {"decrypt", 'd',    NULL,     0,  "Decrypt the file instead"}
};

static char doc[] = "Encode a file into a (Bitmap) picture!";
static char args_doc[] = "bitmapper [flags] filename";

struct args_t {
    char *oname;
    char *iname;
    short depth;
    int dec;
};

void destruct(struct args_t *args) {
    if (args->oname)
        free(args->oname);
    if (args->iname)
        free(args->iname);
}

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
    struct args_t *args = (struct args_t*)state->input;
    switch (key) {
        case 'o':
            args->oname = (char*)malloc(strlen(arg));
            strcpy(args->oname, arg);
            break;
        case 'c':
            args->depth = atoi(arg);
            if (args->depth < 1 || args->depth > 32) {
                printf("Depth must be between 1 and 32");
                exit(0);
            }
            break;
        case 'd':
            args->dec = 1;
            break;
        case ARGP_KEY_END:
            if (!args->iname) {
                printf("I suggest you use --help.\n");
                destruct(args);
                exit(0);
            }
            break;
        case ARGP_KEY_ARG:
            args->iname = (char*)malloc(strlen(arg));
            strcpy(args->iname, arg);
            break;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = {opts, parse_opt, args_doc, doc};

int main(int argc, char **argv) {
    struct args_t args;
    args.oname = (char*)malloc(8);
    strcpy(args.oname, "out.bmp");
    args.depth = 24;
    args.iname = NULL;
    args.dec = 0;
    argp_parse(&argp, argc, argv, 0, 0, &args);

    FILE *in = fopen(args.iname, "rb");
    FILE *out = fopen(args.oname, "wb");

    destruct(&args);

    if (!in) {
        printf("That file doesn't exist.\n");
        exit(0);
    }
    if (args.dec)
        decode_bmp(in, out);
    else
        encode_bmp(in, out, args.depth);

    fclose(in);
    fclose(out);

}

