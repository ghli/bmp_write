#ifndef BMP_WRITER
#define BMP_WRITER
#include <stdio.h>

unsigned char get_byte(long, int);
void encode_bmp(FILE *, FILE *, int);
void decode_bmp(FILE *, FILE *);
#endif
