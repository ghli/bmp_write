CXX=gcc

INC=include
BIN=bin
SRC=src
ODIR=obj
TEST=test
EXT=.c
NAME=bmpwriter

OBJECTS=$(patsubst ${SRC}/%${EXT}, ${ODIR}/%.o, $(shell find ${SRC}/ -name '*'${EXT} -not -name "main${EXT}"))

CFLAGS=-I${INC} -fPIC -lm

# Regular src objects
${ODIR}/%.o : ${SRC}/%${EXT}
	@mkdir -p ${ODIR}
	$(CXX) -c -o $@ $^ ${CFLAGS}

main : ${OBJECTS}
	@mkdir -p ${BIN}
	$(CXX) -o ${BIN}/${NAME} ${SRC}/main${EXT} $^ ${CFLAGS}

.PHONY : clean all ${NAME}

${NAME} : main

clean :
	rm -rf $(ODIR)
	rm -rf $(BIN)

all : main

remake : clean all
