#include "bmpwriter.h"
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <math.h>

#define TWOBYTE_ZERO 0x00, 0x00
#define FOURBYTE_ZERO 0x00, 0x00, 0x00, 0x00

unsigned char get_byte(long x, int n) {
    return (x >> 8*n) & 0xff;
}

void encode_bmp(FILE *in, FILE *out, int depth) {
    fseek(in, 0L, SEEK_END);
    unsigned long size = ftell(in);
    rewind(in);
    
    unsigned int pic_dim = (unsigned int)ceil(sqrt((double)size*8/depth));

    unsigned long row_size = ceil((double)depth*pic_dim/32) * 4;
    unsigned long tot_size = row_size * pic_dim;
    unsigned long buf_bytes = tot_size - size;
    /**
     * Header stuff.
     * 0x0  2byte FD - std, NI
     * 0x2  4byte filesize - std, NI
     * 0x6  4byte our signature - custom, I
     * 0xa  4byte data offset location - std, NI
     * 0xe  4byte header size - std, NI
     * 0x12 4byte width - std, NI
     * 0x16 4byte height - std, NI
     * 0x1a 2byte color planes - std, NI
     * 0x1c 2byte depth - std, I
     * 0x1e 4byte compression method - std, NI
     * 0x22 4byte size of raw data - std, NI
     * 0x26 8byte number of filler pixels - custom, I
     * 0x2e 4byte number of colors - std, NI
     * 0x32 4byte number of I colors - std, NI
     * 0x36 nbyte start of image data. - std, I
     */
    // Little Endian Header
    unsigned char header[0x36] = {0x42, 0x4d,
        get_byte(54+tot_size,0),get_byte(54+tot_size,1), get_byte(54+tot_size,2), get_byte(54+tot_size,3),
        0xef, 0xbe, 0xad, 0xde, // 0xdeadbeef, our signature
        0x36, 0x00, TWOBYTE_ZERO,
        0x28, 0x00, TWOBYTE_ZERO,
        get_byte(pic_dim, 0), get_byte(pic_dim, 1), get_byte(pic_dim, 2), get_byte(pic_dim, 3),
        get_byte(pic_dim, 0), get_byte(pic_dim, 1), get_byte(pic_dim, 2), get_byte(pic_dim, 3),
        0x01, 0x00,
        (unsigned char) (depth & 0xff), 0x00,
        FOURBYTE_ZERO,
        get_byte(tot_size,0), get_byte(tot_size,1), get_byte(tot_size,2), get_byte(tot_size,3),
        get_byte(buf_bytes, 0), get_byte(buf_bytes, 1), get_byte(buf_bytes, 2), get_byte(buf_bytes, 3), get_byte(buf_bytes, 4), get_byte(buf_bytes, 5), get_byte(buf_bytes, 6), get_byte(buf_bytes, 7),
        FOURBYTE_ZERO,
        FOURBYTE_ZERO};

    // Write the header.
    for (int i = 0; i < 0x36; ++i) {
        fwrite(&header[i], 1, 1, out);
    }
    // Write the padding.
    unsigned char fill = 0x00;
    for (unsigned long i = 0; i < buf_bytes; ++i) {
        fwrite(&fill, 1, 1, out);
    }
    // Write the data.
    for (unsigned long i = 0; i < size; ++i) {
        fread(&fill, 1, 1, in);
        fwrite(&fill, 1, 1, out);
    }
}

void decode_bmp(FILE *in, FILE *out) {
    /**
     * Header stuff.
     * 0x0  2byte FD - std, NI
     * 0x2  4byte filesize - std, NI
     * 0x6  4byte our signature - custom, I
     * 0xa  4byte data offset location - std, NI
     * 0xe  4byte header size - std, NI
     * 0x12 4byte width - std, NI
     * 0x16 4byte height - std, NI
     * 0x1a 2byte color planes - std, NI
     * 0x1c 2byte depth - std, I
     * 0x1e 4byte compression method - std, NI
     * 0x22 4byte size of raw data - std, NI
     * 0x26 8byte number of filler pixels - custom, I
     * 0x2e 4byte number of colors - std, NI
     * 0x32 4byte number of I colors - std, NI
     * 0x36 nbyte start of image data. - std, I
     */

    // Get our signature.
    int sig;
    fseek(in, 0x6, SEEK_SET);
    fread(&sig, 4, 1, in);

    // If it doesn't have our signature, it's not generated with us, so we can't read it.
    if (sig != 0xdeadbeef) {
        printf("Picture not created with this tool. We got %x", sig);
        return;
    }

    // Filler pixels to skip.
    unsigned long filler_ct = 0;
    fseek(in, 0x26, SEEK_SET);
    if (!fread(&filler_ct, 8, 1, in)) {
        printf("Offset's borked");
        return;
    }

    // Read our important data.
    fseek(in, 0x36+filler_ct, SEEK_SET);
    //printf("%lu filler bytes.\n", filler_ct);
    char dat;
    while(fread(&dat, 1, 1, in)) {
        fwrite(&dat, 1, 1, out);
    }
}
