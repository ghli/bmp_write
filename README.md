# BMPWRITER
A basic [steganography](https://en.wikipedia.org/wiki/Steganography) tool.  
Only works on little endian systems.
# Installation
Run `make`  
Add `bmpwriter` executable into appropriate `PATH`
# Usage
`bmpwriter [options] filename`
